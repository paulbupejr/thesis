#!/usr/bin/python
from __future__ import division

__author__ = 'cri_marketing'
import math
from droneapi.lib import VehicleMode
from pymavlink import mavutil
from argparse import ArgumentParser

parser = ArgumentParser(description=__doc__)


parser.add_argument("--baudrate", type=int,
                  help="master port baud rate", default=57600)
parser.add_argument("--device", dest="device", help="serial device", default="/dev/ttyACM0")
args = parser.parse_args()

# create a mavlink serial instance
master = mavutil.mavlink_connection(args.device, baud=args.baudrate)





# launch = [33.423253, -83.786715]
#
# def get_coordinate(origin, distance, bearing):
#     lat = math.radians(origin[0])
#     lng = math.radians(origin[1])
#     dist = distance / 6371  # radius of the earth in km. Angular distance in radians
#     brng = math.radians(bearing)
#     finlat = math.asin(math.sin(lat) * math.cos(dist) + math.cos(lat) * math.sin(dist)*math.cos(brng))
#     finlong = lng + math.atan2(math.sin(brng)*math.sin(dist)*math.cos(lat),
#                                math.cos(dist)-math.sin(lat)*math.sin(finlat))
#     finlong = (finlong+3*math.pi) % (2*math.pi) - math.pi  # normalise to -180..+180 degrees
#     return [round(math.degrees(finlat), 6), round(math.degrees(finlong), 6)]
#
# c2_origin = get_coordinate(launch, 2, 90)
# print get_coordinate(launch, 1, 60)
# print c2_origin
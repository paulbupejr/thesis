from math import radians
from drone import gps_newpos
import operator, random

diff = [1, 2, 5, 6, 8, 10]

def verifyOrder(list):

    total = len(list)
    count = 1
    for i in list:
        if count == total:

            return

    pass

def cluster(center, offset, radius):
    """
    Generates coordinates based on a hexagonal pattern with a central drone
    :param center:
    :param offset:
    :param radius:
    :return:
    """
    lat = float(center[0])
    lon = float(center[1])
    cluster = {}
    cluster[1] = (lat, lon)
    cluster[2] = gps_newpos(lat, lon, 0+offset, radius)
    cluster[3] = gps_newpos(lat, lon, 60+offset, radius)
    cluster[4] = gps_newpos(lat, lon, 120+offset, radius)
    cluster[5] = gps_newpos(lat, lon, 180+offset, radius)
    cluster[6] = gps_newpos(lat, lon, 240+offset, radius)
    cluster[7] = gps_newpos(lat, lon, 300+offset, radius)
    return cluster

def clusters(dist, center, drones):
    """
    Assigns each drone a destination coordinate based on its position in the drones array
    :param dist:
    :param center:
    :param drones:
    :return:
    """
    d = dist
    reuse_distance = d * 3
    offset = 19.11  # calculated value
    # offset = 15.11  # calculated value
    centers = cluster(center, offset, reuse_distance)
    total = len(drones)
    cluster_count = 1
    current_cluster = cluster(center, offset, d)
    master_cluster_count = 1

    sorted_drones = sorted(drones, key=operator.attrgetter('id'))  # Sort drones by ID attribute

    for i in range(1, total+1):
        center = centers[master_cluster_count]
        current_cluster = cluster(center, 0, d)
        temp = current_cluster[cluster_count]
        # print "Center: %.6f,%.6f | %.6f,%.6f" % (center[0], center[1], temp[0], temp[1])
        # print "%.6f,%.6f" % (temp[0], temp[1])

        # sorted_drones[i-1] .destination = ("[%.6f,%.6f]" % (temp[0], temp[1]))
        destination = ["%.7f" % temp[0], "%.7f" % temp[1]]
        sorted_drones[i-1] .destination = [float(destination[0]), float(destination[1])]

        if cluster_count == 7:
            master_cluster_count += 1
            cluster_count = 0

        cluster_count += 1

def simClusters(dist, center, drones):
    """
    Assigns each drone a destination coordinate based on its position in the drones array
    :param dist:
    :param center:
    :param drones:
    :return:
    """
    d = dist
    reuse_distance = d * 1.7435
    # offset = 19.11  # calculated value
    offset = 30.11  # calculated value
    centers = cluster(center, offset, reuse_distance)
    total = drones
    cluster_count = 1
    current_cluster = cluster(center, offset, d)
    master_cluster_count = 1

    cords = []

    for i in range(1, total+1):
        center = centers[master_cluster_count]
        current_cluster = cluster(center, 0, d)
        temp = current_cluster[cluster_count]
        # print "Center: %.6f,%.6f | %.6f,%.6f" % (center[0], center[1], temp[0], temp[1])
        # print "%.6f,%.6f" % (temp[0], temp[1])

        # sorted_drones[i-1] .destination = ("[%.6f,%.6f]" % (temp[0], temp[1]))
        destination = ["%.7f" % temp[0], "%.7f" % temp[1]]
        destination = [float(destination[0]), float(destination[1])]
        cords.append(destination)

        if cluster_count == 7:
            master_cluster_count += 1
            cluster_count = 0

        cluster_count += 1

    simDrones = []
    thisCount = 1
    for n in cords:
      temp = {
            "id": thisCount,
            "gps": n,
            "sats": random.randrange(5,16),
            "gps_fix": "GPS Fix",
            "alt": float("%.1f" % random.uniform(19,22)),
            "alt2": 20,
            "mode": "Loiter",
            "armed": "Armed",
            "dest": n,
            "voltage": float("%.1f" % random.uniform(10,12)),
            "batt": random.randrange(90,100),
            "port": "COM%i" % thisCount,
            "state": "Operation"
            }
      simDrones.append(temp)
      thisCount += 1
    return simDrones

# def two():
#     lat = home[0]
#     lon = home[1]
#     x1 = gps_newpos(lat, lon, 90, dist/2)
#     x2 = gps_newpos(lat, lon, 270, dist/2)
#     return [x1, x2]
#
# def three():
#     lat = home[0]
#     lon = home[1]
#     r = (dist * math.sqrt(3)) / 3
#     x1 = gps_newpos(lat, lon, 0, r)
#     x2 = gps_newpos(lat, lon, 120, r)
#     x3 = gps_newpos(lat, lon, 240, r)
#     return [x1, x2, x3]
#
# def four():
#     lat = home[0]
#     lon = home[1]
#     r = dist / 2
#     x1 = gps_newpos(lat, lon, 45, r)
#     x2 = gps_newpos(lat, lon, 135, r)
#     x3 = gps_newpos(lat, lon, 225, r)
#     x4 = gps_newpos(lat, lon, 315, r)
#     return [x1, x2, x3, x4]
#
# def five():
#     lat = home[0]
#     lon = home[1]
#     r = dist / 2
#     x1 = [(lat, lon)]
#     x2 = gps_newpos(lat, lon, 45, r)
#     x3 = gps_newpos(lat, lon, 135, r)
#     x4 = gps_newpos(lat, lon, 225, r)
#     x5 = gps_newpos(lat, lon, 315, r)
#     return [x1, x2, x3, x4, x5]
#
# def six():
#     lat = home[0]
#     lon = home[1]
#     r = dist / 2 * math.sin(36)
#     x1 = (lat, lon)
#     x2 = gps_newpos(lat, lon, 0, r)
#     x3 = gps_newpos(lat, lon, 72, r)
#     x4 = gps_newpos(lat, lon, 144, r)
#     x5 = gps_newpos(lat, lon, 216, r)
#     x6 = gps_newpos(lat, lon, 288, r)
#     return [x1, x2, x3, x4, x5, x6]
#






# clusters()
# b = five()
# cprint(b)

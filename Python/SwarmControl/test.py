import time, \
    threading, \
    traceback, \
    math, \
    os, \
    sys
from globals import *
from pymavlink import mavutil
from itertools import count
from  stateMachine import *
location = mavutil.location

class Commands(object):
    """Commands to control drone.
       Partial source from DroneAPI
    """
    def __init__(self, vehicle, module):
        self.master = vehicle
        self.module = module

    def set_stream_rate(self, rate=1):
        """Sets the stream rate of the mavlink connection, varying from 1 to 20"""
        self.master.mav.request_data_stream_send(self.master.target_system, self.master.target_component,
                                                mavutil.mavlink.MAV_DATA_STREAM_ALL, 2, rate)
    def waitAck(self):
        ack_msg = None
        while (ack_msg is None or ack_msg.result != 0):
            logging.debug("waiting for acknowledgement..")
            ack_msg = self.master.recv_match(type='COMMAND_ACK')
            time.sleep(0.01)
        logging.debug("Acknowledgement received : Command = " + str(ack_msg.command) + ", result = " + str(ack_msg.result))

    def mav_param_set(self, name, value, retries=3):
        '''set a parameter on a mavlink connection'''
        mav = self.master
        got_ack = False
        while retries > 0 and not got_ack:
            retries -= 1
            mav.param_set_send(name.upper(), float(value))
            tstart = time.time()
            while time.time() - tstart < 1:
                ack = mav.recv_match(type='PARAM_VALUE', blocking=False)
                logging.debug(ack)
                if ack == None:
                    time.sleep(0.1)
                    continue
                if str(name).upper() == str(ack.param_id).upper():
                    got_ack = True
                    logging.debug("Set: " + name, float(value))
                    break
        if not got_ack:
            logging.debug("timeout setting %s to %f" % (name, float(value)))
            return False
        return True

    def takeoff(self):
        self.mode("Stabilize")
        """Take off to the provided altitude"""
        alt = self.module.config_alt
        if alt is not None:
            altitude = float(alt)
            self.master.mav.command_long_send(self.master.target_system,
                                              self.master.target_component,
                                              mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                                              0, 0, 0, 0, 0, 0, 0,
                                              altitude)
            logging.debug("Taking Off...")

    def goto(self, l):
        if l.is_relative:
            frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
        else:
            frame = mavutil.mavlink.MAV_FRAME_GLOBAL
        self.master.mav.mission_item_send(self.master.target_system,
                                          self.master.target_component,
                                          0,
                                          frame,
                                          mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                                          2, 0, 0, 0, 0, 0,
                                          l.lat, l.lon, l.alt)

    def mode(self, mode):
        """Mode Setter"""
        if mode == "Auto":
            logging.debug("MAV %i: Change to Auto", self.module.id)
            self.master.set_mode("Auto")
            wait_mode(self.master,"Auto",3)
        elif mode == "Stabilize":
            logging.debug("MAV %i: Change to Stabilize", self.module.id)
            self.master.set_mode("STABILIZE")
            wait_mode(self.master,"Stabilize",3)
        elif mode == "Guided":
            logging.debug("MAV %i: Change to Guided", self.module.id)
            self.master.set_mode("GUIDED")
            wait_mode(self.master,"Guided",3)
        elif mode == "RTL":
            logging.debug("MAV %i: Change to RTL", self.module.id)
            self.master.set_mode_rtl()
            wait_mode(self.master,"RTL",3)
        elif mode == "Loiter":
            logging.debug("MAV %i: Change to Loiter", self.module.id)
            self.master.set_mode_loiter()
            wait_mode(self.master,"Loiter",3)
        else:
            pass

    def arm(self):
        """Arm autopilot"""
        logging.debug("MAV %i: Arming", self.module.id)
        self.master.arducopter_arm()
        # ack_msg = self.master.recv_match(type='COMMAND_ACK', blocking=True, timeout=2000)
        # if ack_msg is not None:
        #     logging.debug("Acknowledgement received : Command = " + str(ack_msg.command)
        #                   + ", result = " + str(ack_msg.result))
        # self.master.motors_armed_wait()
        logging.debug("MAV %i: Armed", self.module.id)

    def disarm(self):
        """Disarms autopilot"""
        self.master.arducopter_disarm()
        # self.master.motors_disarmed_wait()

    def wait_heartbeat(self):
        '''wait for a heartbeat so we know the target system IDs'''
        logging.debug("Waiting for APM heartbeat")
        self.master.wait_heartbeat()
        logging.debug("Heartbeat from APM (system %u component %u)" % (self.master.target_system, self.master.target_system))

    def check_link(self):
        tnow = time.time()
        if tnow > self.module.last_message + 3 or self.master.portdead:
            self.module.link_error = True
        else:
            self.module.link_error = False

    def send_heartbeat(self):
        '''MAVProxy function to send heartbeat to vehicle'''
        if self.master.mavlink10():
            self.master.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_GCS, mavutil.mavlink.MAV_AUTOPILOT_INVALID,
                                           0, 0, 0)
        else:
            MAV_GROUND = 5
            MAV_AUTOPILOT_NONE = 4
            self.master.mav.heartbeat_send(MAV_GROUND, MAV_AUTOPILOT_NONE)


class Drone(object):
    ids = count(1)  # Assigns IDs to drones
    drones = []  # Universal container for all drones
    def __init__(self, connection, altitude, port):
        self.vehicle = connection  # Vehicle instance
        self.commands = Commands(self.vehicle, self)  # Create instance of commands class
        self.port = port
        self.exit = False

        self.id = self.ids.next()  # Session ID for this drones
        self.drones.append(self)  # Add this drone to the master drone list
        self.init_time = time.time()

        self.commands.set_stream_rate()
        self.isArmed = self.vehicle.motors_armed()
        self.flightmode = self.vehicle.flightmode

        self.messages = self.vehicle.messages
        self.last_message = 0
        self.link_error = False

        self.lat = None
        self.lon = None
        self.alt = None
        # self.lat = 32.4195859
        # self.lon = -81.7993319

        self.climb_rate = None
        self.operating_alt = altitude
        self.config_alt = 20

        self.at_config_alt = False   # drone is at configuration altitude
        self.at_op_alt = False  # drone is at operating altitude

        self.fsm = SwarmFSM(self)
        self.state = self.fsm.get_state()

        self.home = None
        self.satellites_visible = None
        self.fix_type = None
        self.destination = None

        self.dist_to_dest = None
        self.dist_to_home = None

        self.last_waypoint = 0

        self.battery_voltage = None
        self.battery_remaining = None

        self.next_thread_num = 0  # Monotonically increasing
        self.threads = {}  # A map from int ID to thread object
        # self.thread = APIThread(self, self.vehicle, "Mav")


    @staticmethod
    def tryConnect(port, operating_alt, baud=57600):
        """This function checks if the serial connection is valid
        before actually returning the class. This should be the only
        function called to connect"""
        try:
            newConnection = mavutil.mavlink_connection(device=port, baud=baud, autoreconnect=False)
            logging.debug("Waiting for APM heartbeat")
            newConnection.wait_heartbeat(blocking=True)
            logging.debug("Connected")

        except:
            logging.debug("Can't connect")
            traceback.print_exc()
            return None
        return Drone(newConnection, operating_alt, port)

    def get_state(self):
        self.state = self.fsm.get_state()

    def packet(self):
        obj = {}
        obj["id"] = self.id
        obj["gps"] = [self.lat, self.lon]
        obj["gps_fix"] = "No GPS Fix" if self.fix_type != 3 else "GPS Fix"
        obj["sats"] = self.satellites_visible
        obj["home"] = self.home
        obj["alt"] = self.alt
        obj["alt2"] = self.config_alt
        obj["mode"] = self.flightmode
        obj["armed"] = "Disarmed" if self.isArmed is False else "Armed"
        obj["dest"] = self.destination
        obj["dist_to_home"] = self.dist_to_home
        obj["dist_to_dest"] = self.dist_to_dest
        obj["voltage"] = self.battery_voltage
        obj["waypoint"] = self.last_waypoint
        obj["batt"] = self.battery_remaining
        obj["port"] = self.port
        obj["state"] = self.state
        obj["link"] = "Link" if self.link_error is False else "Link Error"
        return obj

    def stream(self, vehicle):
        m = vehicle
        '''show incoming mavlink messages'''
        while True:
            if self.exit:
                return
            # msg = m.recv_match(blocking=True)
            # if not msg:
            #     return
            # if msg.get_type() == "BAD_DATA":
            #     if mavutil.all_printable(msg.data):
            #         sys.stdout.write(msg.data)
            #         sys.stdout.flush()
            # else:
            #     logging.debug(msg)
                # self.mavlink_packet(msg)
            # if self.heartbeat_send.trigger():
            #     if debugPeriod.trigger() and debugging:
            #         pass
                    # logging.debug("Heartbeat send")
                # self.commands.send_heartbeat()

            time.sleep(1)
    def calculate(self):
        if self.lat != None and self.destination != None and self.home != None:
            home = location(self.home[0],self.home[1])
            current = location(self.lat, self.lon)
            destination = location(self.destination[0],self.destination[1])
            self.dist_to_dest = float("%.2f" % get_distance(current,destination))
            self.dist_to_home = float("%.2f" % get_distance(current,home))

    def update(self):
        self.vehicle.recv_msg()
        self.last_message = time.time()
        m = self.vehicle.messages
        for typ in m:
            if typ == 'GLOBAL_POSITION_INT':
                (self.vx, self.vy, self.vz) = (m[typ].vx / 100.0, m[typ].vy / 100.0, m[typ].vz / 100.0)
            elif typ == 'GPS_RAW_INT':
                (self.lat, self.lon) = (m[typ].lat / 1.0e7, m[typ].lon / 1.0e7)
                self.satellites_visible = m[typ].satellites_visible
                self.fix_type = m[typ].fix_type
            elif typ == "VFR_HUD":
                self.heading = m[typ].heading
                self.climb_rate = m[typ].climb
                self.alt = "%.2f" % m[typ].alt
            elif typ == "SYS_STATUS":
                self.battery_voltage = m[typ].voltage_battery
                self.battery_remaining = m[typ].battery_remaining
            elif typ == "HEARTBEAT":
                self.flightmode = mavutil.mode_string_v10(m[typ])
                self.isArmed = self.vehicle.motors_armed()
            elif typ in ["WAYPOINT_CURRENT", "MISSION_CURRENT"]:
                self.last_waypoint = m[typ].seq
            elif typ == "HOME":
                self.home = [m[typ].lat / 1.0e7, m[typ].lon / 1.0e7]

    def check_link(self):
        tnow = time.time()
        if tnow > self.last_message + 3 or self.vehicle.portdead:
            self.link_error = True
        else:
            self.link_error = False

def getSerialPorts():
    serial_list = mavutil.auto_detect_serial()
    # serial_list = mavutil.auto_detect_serial(preferred_list=['*FTDI*', "*3D_Robotics*", "*USB_to_UART*"])
    s_list = []
    if len(serial_list) > 0:
        for n in serial_list:
            s_list.append({"port": n.device, "desc": n.description})
            # list[n.device] = n.description
        return s_list

# '''common mavlink utility functions from MAVProxy, ArduPilot,
# PyMavlink, and other credited sources
# '''

radius_of_earth = 6378100.0 # in meters

def wait_mode(mav, mode, timeout=None):
    logging.debug("Waiting for mode %s" % mode)
    mav.recv_match(condition='MAV.flightmode.upper()=="%s".upper()' % mode, timeout=timeout, blocking=True)
    logging.debug("Got mode %s" % mode)
    return mav.flightmode

def get_distance(loc1, loc2):
    '''get ground distance between two locations'''
    dlat = loc2.lat - loc1.lat
    dlong = loc2.lng - loc1.lng
    return math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5

def wait_seconds(seconds_to_wait):
    tstart = time.time()
    tnow = tstart
    while tstart + seconds_to_wait > tnow:
        tnow = time.time()

def wait_altitude(mav, alt_min, alt_max, timeout=30):
    climb_rate = 0
    previous_alt = 0
    '''wait for a given altitude range'''
    tstart = time.time()
    logging.debug("Waiting for altitude between %u and %u" % (alt_min, alt_max))
    while time.time() < tstart + timeout:
        m = mav.recv_match(type='VFR_HUD', blocking=True)
        climb_rate = m.alt - previous_alt
        previous_alt = m.alt
        logging.debug("Wait Altitude: Cur:%u, min_alt:%u, climb_rate: %u" % (m.alt, alt_min , climb_rate))
        if m.alt >= alt_min and m.alt <= alt_max:
            logging.debug("Altitude OK")
            return True
    logging.debug("Failed to attain altitude range")
    return False

def wait_distance(mav, distance, accuracy=5, timeout=30):
    '''wait for flight of a given distance'''
    tstart = time.time()
    start = mav.location()
    while time.time() < tstart + timeout:
        pos = mav.location()
        delta = get_distance(start, pos)
        logging.debug("Distance %.2f meters" % delta)
        if math.fabs(delta - distance) <= accuracy:
            logging.debug("Attained distance %.2f meters OK" % delta)
            return True
        if delta > (distance + accuracy):
            logging.debug("Failed distance - overshoot delta=%f distance=%f" % (delta, distance))
            return False
    logging.debug("Failed to attain distance %u" % distance)
    return False

def wait_location(mav, loc, accuracy=5, timeout=30, target_altitude=None, height_accuracy=-1):
    '''wait for arrival at a location'''
    tstart = time.time()
    if target_altitude is None:
        target_altitude = loc.alt
    logging.debug("Waiting for location %.4f,%.4f at altitude %.1f height_accuracy=%.1f" % (
        loc.lat, loc.lng, target_altitude, height_accuracy))
    while time.time() < tstart + timeout:
        pos = mav.location()
        delta = get_distance(loc, pos)
        logging.debug("Distance %.2f meters alt %.1f" % (delta, pos.alt))
        if delta <= accuracy:
            if height_accuracy != -1 and math.fabs(pos.alt - target_altitude) > height_accuracy:
                continue
            logging.debug("Reached location (%.2f meters)" % delta)
            return True
    logging.debug("Failed to attain location")
    return False

def gps_distance(lat1, lon1, lat2, lon2):
    '''return distance between two points in meters,
    coordinates are in degrees
    thanks to http://www.movable-type.co.uk/scripts/latlong.html'''
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)
    dLat = lat2 - lat1
    dLon = lon2 - lon1

    a = math.sin(0.5*dLat)**2 + math.sin(0.5*dLon)**2 * math.cos(lat1) * math.cos(lat2)
    c = 2.0 * math.atan2(math.sqrt(a), math.sqrt(1.0-a))
    return radius_of_earth * c


def gps_bearing(lat1, lon1, lat2, lon2):
    '''return bearing between two points in degrees, in range 0-360
    thanks to http://www.movable-type.co.uk/scripts/latlong.html'''
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)
    dLat = lat2 - lat1
    dLon = lon2 - lon1
    y = math.sin(dLon) * math.cos(lat2)
    x = math.cos(lat1)*math.sin(lat2) - math.sin(lat1)*math.cos(lat2)*math.cos(dLon)
    bearing = math.degrees(math.atan2(y, x))
    if bearing < 0:
        bearing += 360.0
    return bearing


def wrap_valid_longitude(lon):
    ''' wrap a longitude value around to always have a value in the range
        [-180, +180) i.e 0 => 0, 1 => 1, -1 => -1, 181 => -179, -181 => 179
    '''
    return (((lon + 180.0) % 360.0) - 180.0)

def gps_newpos(lat, lon, bearing, distance):
    '''extrapolate latitude/longitude given a heading and distance
    thanks to http://www.movable-type.co.uk/scripts/latlong.html
    '''
    lat1 = math.radians(lat)
    lon1 = math.radians(lon)
    brng = math.radians(bearing)
    dr = distance/radius_of_earth

    lat2 = math.asin(math.sin(lat1)*math.cos(dr) +
                     math.cos(lat1)*math.sin(dr)*math.cos(brng))
    lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(dr)*math.cos(lat1),
                             math.cos(dr)-math.sin(lat1)*math.sin(lat2))
    return (math.degrees(lat2), wrap_valid_longitude(math.degrees(lon2)))

def gps_offset(lat, lon, east, north):
    '''return new lat/lon after moving east/north
    by the given number of meters'''
    bearing = math.degrees(math.atan2(east, north))
    distance = math.sqrt(east**2 + north**2)
    return gps_newpos(lat, lon, bearing, distance)


def mkdir_p(dir):
    '''like mkdir -p'''
    if not dir:
        return
    if dir.endswith("/") or dir.endswith("\\"):
        mkdir_p(dir[:-1])
        return
    if os.path.isdir(dir):
        return
    mkdir_p(os.path.dirname(dir))
    try:
        os.mkdir(dir)
    except Exception:
        pass

def polygon_load(filename):
    '''load a polygon from a file'''
    ret = []
    f = open(filename)
    for line in f:
        if line.startswith('#'):
            continue
        line = line.strip()
        if not line:
            continue
        a = line.split()
        if len(a) != 2:
            raise RuntimeError("invalid polygon line: %s" % line)
        ret.append((float(a[0]), float(a[1])))
    f.close()
    return ret


def polygon_bounds(points):
    '''return bounding box of a polygon in (x,y,width,height) form'''
    (minx, miny) = (points[0][0], points[0][1])
    (maxx, maxy) = (minx, miny)
    for p in points:
        minx = min(minx, p[0])
        maxx = max(maxx, p[0])
        miny = min(miny, p[1])
        maxy = max(maxy, p[1])
    return (minx, miny, maxx-minx, maxy-miny)

def bounds_overlap(bound1, bound2):
    '''return true if two bounding boxes overlap'''
    (x1,y1,w1,h1) = bound1
    (x2,y2,w2,h2) = bound2
    if x1+w1 < x2:
        return False
    if x2+w2 < x1:
        return False
    if y1+h1 < y2:
        return False
    if y2+h2 < y1:
        return False
    return True

def degrees_to_dms(degrees):
    '''return a degrees:minutes:seconds string'''
    deg = int(degrees)
    min = int((degrees - deg)*60)
    sec = ((degrees - deg) - (min/60.0))*60*60
    return u'%d\u00b0%02u\'%05.2f"' % (deg, abs(min), abs(sec))
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import threading
import traceback
import math
import os
import sys
from pymavlink import mavutil
from itertools import count
from  stateMachine import *


def getSerialPorts():
    serial_list = mavutil.auto_detect_serial(preferred_list=['*FTDI*', "*3D_Robotics*", "*USB_to_UART*"])
    list = []
    if len(serial_list) > 0:
        for n in serial_list:
            list.append({"port": n.device, "desc": n.description})
            # list[n.device] = n.description
        return list

class APIThread(threading.Thread):
    """Threading class from DroneAPI"""
    def __init__(self, module, fn, con, description):
        super(APIThread, self).__init__()
        self.module = module
        self.description = description
        self.exit = False  # Python has no standard way to kill threads, this allows
        self.fn = fn
        self.con = con
        self.thread_num = module.next_thread_num
        module.next_thread_num = module.next_thread_num + 1
        self.daemon = True  # For now I think it is okay to let mavproxy exit if api clients are still running
        self.start()
        self.name = "APIThread-%s" % self.thread_num
        self.module.thread_add(self)

    def kill(self):
        """Ask the thread to exit.  The thread must check threading.current_thread().exit periodically"""
        print "Asking %s to exit..." % self.name
        self.exit = True

    def run(self):
        try:
            self.fn()
            print("%s exiting..." % self.name)
        except Exception as e:
            print("Exception in %s: %s" % (self.name, str(e)))
            traceback.print_exc()
        self.module.thread_remove(self)

    def __str__(self):
        return "%s: %s" % (self.thread_num, self.description)

class Drone(object):
    ids = count(1)
    drones = []

    def __init__(self, connection, port):
        self.error = None
        self.id = self.ids.next()
        self.drones.append(self)
        self.init_time = time.time()

        self.vehicle = connection
        self.port = port
        self.isArmed = None
        self.flightmode = None
        self.command = None

        self.fsm = SwarmFSM(self)
        self.state = self.fsm.state
        # self.state = None

        self.last_message = 0
        self.link_error = False

        self.lat = None
        self.lon = None
        self.alt = None
        self.alt2 = 30
        self.position = {"lat": self.lat, "lon": self.lon, "alt": self.alt, "alt2": self.alt2}

        self.eph = None
        self.epv = None
        self.satellites_visible = None
        self.fix_type = None
        self.destination = None
        self.home = None
        self.last_waypoint = 0

        self.vx = None
        self.vy = None
        self.vz = None

        self.battery_voltage = None
        self.battery_remaining = None

        self.airspeed = None
        self.groundspeed = None

        self.pitch = None
        self.yaw = None
        self.roll = None
        self.pitchspeed = None
        self.yawspeed = None
        self.rollspeed = None

        self.rc_readback = {}

        self.next_thread_num = 0  # Monotonically increasing
        self.threads = {}  # A map from int ID to thread object

        self.wait_heartbeat()
        print("Waiting for GPS Fix")

        self.set_stream_rate()
        # self.vehicle.wait_gps_fix()
        APIThread(self, self.show_messages, self.vehicle, "Mav1")
        self.show_messages()
        print("Vehicle Connected")

    @staticmethod
    def tryConnect(port, baud=57600):
        """This function checks if the serial connection is valid
        before actually returning the class. This should be the only
        function called to connect"""
        try:
          newConnection = mavutil.mavlink_connection(device=port, baud=baud, autoreconnect=True)

        except:
            print "Can't connect"
            traceback.print_exc()
            return None
        return Drone(newConnection, port)

    # ++++++++++State Machine Methods++++++++++


    def set_stream_rate(self):
        """Sets the stream rate of the mavlink connection, varying from 1 to 20"""
        master = self.vehicle
        master.mav.request_data_stream_send(master.target_system, master.target_component,
                                                mavutil.mavlink.MAV_DATA_STREAM_ALL, 2, 1)

    def packet(self):

        obj = {}
        obj["id"] = self.id
        obj["gps"] = [self.lat, self.lon]
        obj["gps_fix"] = "No GPS Fix" if self.fix_type != 3 else "GPS Fix"
        obj["sats"] = self.satellites_visible
        obj["home"] = self.home
        obj["alt"] = self.alt
        obj["alt2"] = self.alt2
        obj["mode"] = self.flightmode
        obj["armed"] = "Disarmed" if self.isArmed is False else "Armed"
        obj["dest"] = self.destination
        obj["voltage"] = self.battery_voltage
        obj["batt"] = self.battery_remaining
        obj["port"] = self.port
        obj["state"] = self.state
        obj["link"] = "Link" if self.link_error is False else "Link Error"
        print obj["alt"]
        return obj

    def mode_auto(self):
        self.vehicle.set_mode_auto()

    def mode_stabalize(self):
        self.vehicle.set_mode("STABILIZE")

    def mode_guided(self):
        self.vehicle.set_mode("GUIDED")

    def mode_rtl(self):
        self.vehicle.set_mode_rtl()

    def mode_manual(self):
        self.vehicle.set_mode_manual()

    def arm(self):
        self.vehicle.arducopter_arm()
        self.vehicle.motors_armed_wait()

    def disarm(self):
        self.vehicle.arducopter_disarm()
        self.vehicle.motors_disarmed_wait()

    def takeoff(self, alt=None):
        alt = self.alt2
        master = self.vehicle
        if alt is not None:
            altitude = float(alt)
            master.mav.command_long_send(master.target_system,
                                                    master.target_component,
                                                    mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,
                                                    0, 0, 0, 0, 0, 0, 0,
                                                    altitude)

    def goto(self, l):
        master = self.vehicle
        if l.is_relative:
            frame = mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT
        else:
            frame = mavutil.mavlink.MAV_FRAME_GLOBAL
            master.mav.mission_item_send(master.target_system,
                                                master.target_component,
                                                0,
                                                frame,
                                                mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                                                2, 0, 0, 0, 0, 0,
                                                l.lat, l.lon, l.alt)

    def gotoConfig(self):
        """Tells the drone to go to it's configuration height"""
        master = self.vehicle
        frame = mavutil.mavlink.MAV_FRAME_GLOBAL
        master.mav.mission_item_send(master.target_system,
                                            master.target_component,
                                            0,
                                            frame,
                                            mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                                            2, 0, 0, 0, 0, 0,
                                            self.position["lat"], self.position["lon"], self.position["alt2"])

    def wait_heartbeat(self):
        m = self.vehicle
        '''wait for a heartbeat so we know the target system IDs'''
        print("Waiting for APM heartbeat")
        m.wait_heartbeat()
        print("Heartbeat from APM (system %u component %u)" % (m.target_system, m.target_system))

    def send_heartbeat(self):
        '''MAVProxy function to send heartbeat to vehicle'''
        master = self.vehicle
        if master.mavlink10():
            master.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_GCS, mavutil.mavlink.MAV_AUTOPILOT_INVALID,
                                      0, 0, 0)
        else:
            MAV_GROUND = 5
            MAV_AUTOPILOT_NONE = 4
            master.mav.heartbeat_send(MAV_GROUND, MAV_AUTOPILOT_NONE)

    def check_link(self):
        tnow = time.time()
        if tnow > self.last_message + 3 or self.vehicle.portdead:
            self.link_error = True
        else:
            self.link_error = False

    def show_messages(self):
        m = self.vehicle
        '''show incoming mavlink messages'''
        while True:
            msg = m.recv_match(blocking=True)
            if not msg:
                return
            if msg.get_type() == "BAD_DATA":
                if mavutil.all_printable(msg.data):
                    sys.stdout.write(msg.data)
                    sys.stdout.flush()
            else:
                # print(msg)
                self.mavlink_packet(msg)
        time.sleep(0.01)

    def mavlink_packet(self, m):
        typ = m.get_type()
        self.last_message = time.time()
        if typ == 'GLOBAL_POSITION_INT':
            (self.lat, self.lon) = (m.lat / 1.0e7, m.lon / 1.0e7)
            (self.vx, self.vy, self.vz) = (m.vx / 100.0, m.vy / 100.0, m.vz / 100.0)
            # self.__on_change('location', 'velocity')
        # elif typ == 'GPS_RAW':
        #     pass
        #     # better to just use global position int
        #     # (self.lat, self.lon) = (m.lat, m.lon)
        #     # self.__on_change('location')
        elif typ == 'GPS_RAW_INT':
            # (self.lat, self.lon) = (m.lat / 1.0e7, m.lon / 1.0e7)
            self.eph = m.eph
            self.epv = m.epv
            self.satellites_visible = m.satellites_visible
            self.fix_type = m.fix_type
            # self.__on_change('gps_0')
        elif typ == "VFR_HUD":
            self.heading = m.heading
            self.alt = "%.2f" % m.alt
            self.airspeed = m.airspeed
            self.groundspeed = m.groundspeed
            # self.__on_change('location', 'airspeed', 'groundspeed')
        elif typ == "ATTITUDE":
            self.pitch = m.pitch
            self.yaw = m.yaw
            self.roll = m.roll
            self.pitchspeed = m.pitchspeed
            self.yawspeed = m.yawspeed
            self.rollspeed = m.rollspeed
            # self.__on_change('attitude')
        elif typ == "SYS_STATUS":
            self.battery_voltage = m.voltage_battery
            self.battery_remaining = m.battery_remaining
            # self.__on_change('attitude')
        elif typ == "HEARTBEAT":
            self.flightmode = mavutil.mode_string_v10(m)
            self.isArmed = self.vehicle.motors_armed()
            # self.__on_change('mode', 'armed')
        elif typ in ["WAYPOINT_CURRENT", "MISSION_CURRENT"]:
            self.last_waypoint = m.seq
        elif typ == "RC_CHANNELS_RAW":
            def set(chnum, v):
                '''Private utility for handling rc channel messages'''
                # use port to allow ch nums greater than 8
                self.rc_readback[str(m.port * 8 + chnum)] = v

            set(1, m.chan1_raw)
            set(2, m.chan2_raw)
            set(3, m.chan3_raw)
            set(4, m.chan4_raw)
            set(5, m.chan5_raw)
            set(6, m.chan6_raw)
            set(7, m.chan7_raw)
            set(8, m.chan8_raw)
        self.packet()


    def heightIsWithinMargin(self, margin=1):
        """Returns true if the two altitudes are within a certain
        margin in meters"""
        a = self.alt
        b = self.alt2
        margin = margin
        difference = abs(a - b)
        if difference < margin:
          return True
        if difference >= margin:
          return False

    def thread_remove(self, t):
        del self.threads[t.thread_num]

    def thread_add(self, t):
        self.threads[t.thread_num] = t






# '''common mavproxy utility functions'''

radius_of_earth = 6378100.0 # in meters

def gps_distance(lat1, lon1, lat2, lon2):
    '''return distance between two points in meters,
    coordinates are in degrees
    thanks to http://www.movable-type.co.uk/scripts/latlong.html'''
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)
    dLat = lat2 - lat1
    dLon = lon2 - lon1

    a = math.sin(0.5*dLat)**2 + math.sin(0.5*dLon)**2 * math.cos(lat1) * math.cos(lat2)
    c = 2.0 * math.atan2(math.sqrt(a), math.sqrt(1.0-a))
    return radius_of_earth * c


def gps_bearing(lat1, lon1, lat2, lon2):
    '''return bearing between two points in degrees, in range 0-360
    thanks to http://www.movable-type.co.uk/scripts/latlong.html'''
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)
    lon2 = math.radians(lon2)
    dLat = lat2 - lat1
    dLon = lon2 - lon1
    y = math.sin(dLon) * math.cos(lat2)
    x = math.cos(lat1)*math.sin(lat2) - math.sin(lat1)*math.cos(lat2)*math.cos(dLon)
    bearing = math.degrees(math.atan2(y, x))
    if bearing < 0:
        bearing += 360.0
    return bearing


def wrap_valid_longitude(lon):
    ''' wrap a longitude value around to always have a value in the range
        [-180, +180) i.e 0 => 0, 1 => 1, -1 => -1, 181 => -179, -181 => 179
    '''
    return (((lon + 180.0) % 360.0) - 180.0)

def gps_newpos(lat, lon, bearing, distance):
    '''extrapolate latitude/longitude given a heading and distance
    thanks to http://www.movable-type.co.uk/scripts/latlong.html
    '''
    lat1 = math.radians(lat)
    lon1 = math.radians(lon)
    brng = math.radians(bearing)
    dr = distance/radius_of_earth

    lat2 = math.asin(math.sin(lat1)*math.cos(dr) +
                     math.cos(lat1)*math.sin(dr)*math.cos(brng))
    lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(dr)*math.cos(lat1),
                             math.cos(dr)-math.sin(lat1)*math.sin(lat2))
    return (math.degrees(lat2), wrap_valid_longitude(math.degrees(lon2)))

def gps_offset(lat, lon, east, north):
    '''return new lat/lon after moving east/north
    by the given number of meters'''
    bearing = math.degrees(math.atan2(east, north))
    distance = math.sqrt(east**2 + north**2)
    return gps_newpos(lat, lon, bearing, distance)


def mkdir_p(dir):
    '''like mkdir -p'''
    if not dir:
        return
    if dir.endswith("/") or dir.endswith("\\"):
        mkdir_p(dir[:-1])
        return
    if os.path.isdir(dir):
        return
    mkdir_p(os.path.dirname(dir))
    try:
        os.mkdir(dir)
    except Exception:
        pass

def polygon_load(filename):
    '''load a polygon from a file'''
    ret = []
    f = open(filename)
    for line in f:
        if line.startswith('#'):
            continue
        line = line.strip()
        if not line:
            continue
        a = line.split()
        if len(a) != 2:
            raise RuntimeError("invalid polygon line: %s" % line)
        ret.append((float(a[0]), float(a[1])))
    f.close()
    return ret


def polygon_bounds(points):
    '''return bounding box of a polygon in (x,y,width,height) form'''
    (minx, miny) = (points[0][0], points[0][1])
    (maxx, maxy) = (minx, miny)
    for p in points:
        minx = min(minx, p[0])
        maxx = max(maxx, p[0])
        miny = min(miny, p[1])
        maxy = max(maxy, p[1])
    return (minx, miny, maxx-minx, maxy-miny)

def bounds_overlap(bound1, bound2):
    '''return true if two bounding boxes overlap'''
    (x1,y1,w1,h1) = bound1
    (x2,y2,w2,h2) = bound2
    if x1+w1 < x2:
        return False
    if x2+w2 < x1:
        return False
    if y1+h1 < y2:
        return False
    if y2+h2 < y1:
        return False
    return True

def degrees_to_dms(degrees):
    '''return a degrees:minutes:seconds string'''
    deg = int(degrees)
    min = int((degrees - deg)*60)
    sec = ((degrees - deg) - (min/60.0))*60*60
    return u'%d\u00b0%02u\'%05.2f"' % (deg, abs(min), abs(sec))
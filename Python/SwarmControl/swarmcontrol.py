import sys, struct, time, os
from math import radians
from test import *
from threading import Timer
from pymavlink import mavutil
from flask import Flask, jsonify, render_template, request
from flask.ext.socketio import SocketIO, emit
from globals import *
import swarm

# UI Setup
app = Flask(__name__, static_url_path='')
socketio = SocketIO(app)

#  Global control variables
operating_alt = 20  # meters
center = None
separation = 250  # meters
fence_height = 30
fence_radius = 100

initialized = False
system_command = "Off"

command = "Take Off"  # Main command variable

# print swarm.cluster(center,0,separation)

# mav1 = Drone.tryConnect("com3", operating_alt)

def periodic_tasks(drones):
    """Periodic tasks..."""
    global center
    if center is not None:
        if cluster_period.trigger(): # calculates destinations
            swarm.clusters(separation, center, drones)

    if heartbeat_send.trigger():
        for n in drones:
            pass
            # n.commands.send_heartbeat()

    if heartbeat_check_period.trigger():
        for n in drones:
            n.check_link()

    if sendPeriod.trigger():
        packet = []
        for n in drones:
            # logging.debug(n.packet())
            packet.append(n.packet())
            socketio.emit('status',  {'data': packet})

    # if sendPeriod.trigger():
    #     socketio.emit('status',  {'data': demo})

        # for n in drones:
            # logging.debug(n.packet())
            # packet.append(n.packet())


def drone_loop():

    while True:
        drones = Drone.drones

        for n in drones:
            if n.vehicle.fd is None:
                if n.vehicle.port.inWaiting() > 0:
                    n.update()
                    n.calculate()
                    n.get_state()

        periodic_tasks(drones)


def control_loop():
    global command
    """
    Primary control loop
    :return:
    """
    while True:
        drones = Drone.drones

        if command == "Take Off":
            for n in drones:
                n.fsm.takeOff()
                if n.state == "Ascent":
                    command = "Transition To"
                if debugPeriod.trigger() and debugging:
                    logging.debug("Command: Take Off")
                    logging.debug("State: " + n.state)

        elif command == "Transition To":
            for n in drones:
                if debugPeriod.trigger() and debugging:
                    logging.debug("Command: Transition To")

        elif command == "Settle":
            for n in drones:
                if debugPeriod.trigger() and debugging:
                    logging.debug("Command: Settle")

        elif command == "Transition From":
            for n in drones:
                if debugPeriod.trigger() and debugging:
                    logging.debug("Command: Transition From")

        elif command == "Land":
            for n in drones:
                if debugPeriod.trigger() and debugging:
                    logging.debug("Command: Land")
        else:
            pass
        time.sleep(.01)

def doCommand(command, drone):

    drones = Drone.drones

    if command == "Arm":
        for i in drones:
            if i.id == drone:
                i.commands.arm()

    if command == "Disarm":
        for i in drones:
            if i.id == drone:
                i.commands.disarm()

@app.route('/')
def index():
    return app.send_static_file("index.html")

@socketio.on('get_ports')
def getSerial(message):
    data = getSerialPorts()
    emit('ports', {'data': data})
    # emit('get_ports', {'data': data})

@socketio.on("command")
def execute_command(message):
    command = message["command"].encode('utf-8')
    drone = int(message['drone'].encode('utf-8'))
    doCommand(command, drone)
    logging.debug("%s, %i", command, drone)

@socketio.on("mode")
def execute_command(message):
    drones = Drone.drones

    mode = message["mode"].encode('utf-8')
    drone = int(message['drone'].encode('utf-8'))

    for i in drones:
        if i.id == drone:
            i.commands.mode(mode)
    logging.debug("%s, %i", mode, drone)


@socketio.on("connect")
def add_drone(message):
    port = message["port"].encode('utf-8')
    baud = int(message['baud'].encode('utf-8'))
    print(port, baud)
    Drone.tryConnect(port, operating_alt, baud)

@socketio.on("initiate")
def initiate(message):
    # port = message["port"].encode('utf-8')
    # baud = int(message['baud'].encode('utf-8'))
    control.start()
    # Drone.tryConnect(port, baud)

@socketio.on("simulate")
def simulate(message):
    center = (float(message["lat"]),float(message["lon"]))
    distance = float(message["separation"])
    drones = int(message["drones"])
    simDrones = swarm.simClusters(distance,center,drones)
    logging.debug(simDrones)
    emit('simDrones', {'data': simDrones})

@socketio.on("config")
def set_config(config):
    global center
    global separation
    global fence_height
    global fence_radius
    global operating_alt

    center = (float(config["lat"]),float(config["lon"]))
    separation = float(config["separation"])
    fence_height = float(config["fenceHeight"])
    fence_radius = float(config["fenceRadius"])
    operating_alt = float(config["operatingHeight"])

    logging.debug("config changed to %s", config)
    # Drone.tryConnect(port, baud)

if __name__ == '__main__':
    drone = threading.Thread(name="Drone Loop", target=drone_loop)
    control = threading.Thread(name="Control Loop", target=control_loop)
    drone.setDaemon(True)
    control.setDaemon(True)
    drone.start()
    # control.start()
    # print(mav1.commands.mav_param_set("FENCE_ENABLE",1))

    socketio.run(app)

    sys.exit(1)


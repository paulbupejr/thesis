import sys, struct, time, os
from math import radians
from drone import *
from threading import Timer
from pymavlink import mavutil
from flask import Flask, jsonify, render_template, request
from flask.ext.socketio import SocketIO, emit
import swarm

app = Flask(__name__, static_url_path='')
socketio = SocketIO(app)
# {'sats': 5, 'dest': None, 'gps_fix': 3, 'mode': 'STABILIZE', 'home': None, 'alt': 8.359999656677246, 'armed': False, 'id': 0, 'gps': [32.4195859, -81.7993319]}
# http://www.darrinward.com/lat-long/?id=345080
# [DEBUG] (Drone Loop) {'state': 'Landed', 'sats': 5, 'dist_to_dest': 43.16, 'dest': [32.4192513, -81.7994694], 'gps_fix': 'GPS Fix', 'voltage': 0, 'waypoint': 0, 'alt2': 20, 'port': 'com3', 'dist_to_home': 6.14, 'batt': 96, 'link': 'Link', 'mode': 'STABILIZE', 'home': [32.4196542, -81.7996489], 'alt': '0', 'armed': 'Disarmed', 'id': 1, 'gps': [32.4196106, -81.7996151]}


# Globals
center = (32.419585, -81.799331)
distance = 20
command = "Take Off"
enabled = False
# Drone.tryConnect("COM5", 57600)
# mav1.arm()
mav2 = Drone.tryConnect("com3", 57600)
# mav2.show_messages()

sendPeriod = mavutil.periodic_event(1)
heartbeat_send = mavutil.periodic_event(1)
cluster_period = mavutil.periodic_event(1)
portPeriod = mavutil.periodic_event(.3)

def control_loop():
    while True:
        drones = Drone.drones

        if cluster_period.trigger():
            swarm.clusters(distance, center, drones)

        for n in drones:
            n.check_link()
            if (n.link_error is True):
                pass
                # n.vehicle.close()
                # drones.remove(n)
            # if heartbeat_send.trigger():
            #     n.send_heartbeat()

        if command == "Take Off":
            pass
        elif command == "Transition To":
            pass
        elif command == "Settle":
            pass
        elif command == "Transition From":
            pass
        elif command == "Land":
            pass
        else:
            pass


def sendData():
    while True:
        if portPeriod.trigger():
            ports = getSerialPorts()
            socketio.emit('ports', {'data': ports})
            # print(ports)

        if sendPeriod.trigger():
            drones = Drone.drones
            packet = []
            for n in drones:
                print n.packet()
                packet.append(n.packet())
            socketio.emit('status',  {'data': packet})


def doCommand(command, drone):

    drones = Drone.drones

    if command == "Arm":
        for i in drones:
            if i.id == drone:
                i.arm()

    if command == "Disarm":
        for i in drones:
            if i.id == drone:
                i.disarm()



@app.route('/')
def index():
    return app.send_static_file("index.html")

@socketio.on('get_ports')
def getSerial(message):
    data = getSerialPorts()
    print(message)
    emit('get_ports', {'data': data})

@socketio.on("command")
def execute_command(message):
    command = message["command"].encode('utf-8')
    drone = int(message['drone'].encode('utf-8'))
    print(command, drone)
    doCommand(command, drone)

@socketio.on("connect")
def add_drone(message):
    port = message["port"].encode('utf-8')
    baud = int(message['baud'].encode('utf-8'))
    print(port, baud)
    Drone.tryConnect(port, baud)

@app.route('/_sensors')
def getSensors():
    pass
    # mav1alt = mav1.pitch
    # mav2alt = mav2.pitch
    # return jsonify(mav1alt = mav1alt)

# if __name__ == '__main__':
#     control = threading.Thread(target=control_loop)
#     t = threading.Thread(target=sendData)
#     t.start()
#     control.start()
#     # mav1.arm()
#     print("Initialized")
#     socketio.run(app)


import sys, struct, time, os
from math import radians
from test import *
from threading import Timer
from pymavlink import mavutil
from flask import Flask, jsonify, render_template, request
from flask.ext.socketio import SocketIO, emit
from globals import *
import swarm

mav1 = Drone.tryConnect("com3", 20)
time.sleep(2)
mav1.commands.arm()

while True:
    time.sleep(1)
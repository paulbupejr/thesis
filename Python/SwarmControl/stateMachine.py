from globals import *

fsmDebugging = False

class SwarmFSM(object):
    """This class implements a state machine for handling
    the autonomous swarming
    """
    def __init__(self, drone):
        # State Machine Variables
        self.landedState = LandedState(self, drone)
        self.troubleState = TroubleState(self, drone)
        self.operationState = OperationState(self, drone)
        self.configState = ConfigState(self, drone)
        self.ascentState = AscentState(self, drone)
        self.descentState = DescentState(self, drone)

        self.state = self.landedState

    def get_state(self):
        return self.state.state

    def goto(self,coordinate):
        """
        Calls the goto() method in the current state
        :param coordinate:
        :return:
        """
        self.state.goto(coordinate)

    def takeOff(self):
        """Calls the takeOff() method in the current state"""

        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("FSM: Take Off" + self.state.state)
        self.state.takeOff()

    def land(self):
        """Calls the land() method in the current state"""
        self.state.land()

    def hold(self):
        """Calls the hold() method in the current state"""
        self.state.hold()


    def setState(self, state):
        """Setter method to set the current state"""
        self.state = state

    # ========Getter Methods========
    def getLandedState(self):
        """Getter method to return the state object"""
        return self.landedState

    def getTroubleSate(self):
        """Getter method to return the state object"""
        return self.troubleState

    def getOperationState(self):
        """Getter method to return the state object"""
        return self.operationState

    def getConfigState(self):
        """Getter method to return the state object"""
        return self.configState

    def getAscentState(self):
        """Getter method to return the state object"""
        return self.ascentState

    def getDescentState(self):
        """Getter method to return the state object"""
        return self.descentState

class LandedState(object):
    """Drone is on the ground."""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "Landed"

    def goto(self, coordinate):
        """Travel from one position to another."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def takeOff(self):
        """Takes off to default height."""
        self.drone.commands.takeoff()    #Tell the drone to takeoff
        self.fsm.setState(self.fsm.getAscentState())  # Set the state to "in motion"

    def land(self):
        """Lands the drone at home coordinates."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

class TroubleState(object):
    """Drone has an error"""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "In Trouble"

    def goto(self, coordinate):
        """Travel from one position to another."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is in trouble")

    def takeOff(self):
        """Takes off to default height."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is in trouble")

    def land(self):
        """Lands the drone at home coordinates."""
        self.drone.commands.land()

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is in trouble")


class OperationState(object):
    """Drone is in air and operating normally"""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "Operating"

    def goto(self, coordinate):
        """Travel from one position to another."""
        self.drone.commands.gotoConfig()

    def takeOff(self):
        """Takes off to default height."""
        self.drone.commands.takeoff()    #Tell the drone to takeoff
        self.fsm.setState(self.fsm.getConfigState())  # Set the state to "in motion"

    def land(self):
        """Lands the drone at home coordinates."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")


class ConfigState(object):
    """Drone is moving from one position to another"""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "In Motion"

    def goto(self, coordinate):
        """Travel from one position to another."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def takeOff(self):
        """Takes off to default height."""
        self.drone.commands.takeoff()    #Tell the drone to takeoff
        self.fsm.setState(self.fsm.getConfigState())  # Set the state to "in motion"

    def land(self):
        """Lands the drone at home coordinates."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")


class AscentState(object):
    """Drone is moving from one position to another"""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "Ascent"

    def goto(self, coordinate):
        """Travel from one position to another."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is in ascent")

    def takeOff(self):
        """Takes off to default height."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is already in ascent")

    def land(self):
        """Lands the drone at home coordinates."""
        self.drone.commands.land()
        self.fsm.setState(self.fsm.getDescentState())

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

class DescentState(object):
    """Drone is moving from one position to another"""
    def __init__(self, FSM, drone):
        self.drone = drone
        self.fsm = FSM
        self.state = "Descent"

    def goto(self, coordinate):
        """Travel from one position to another."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is descending")

    def takeOff(self):
        """Takes off to default height."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is descending")

    def land(self):
        """Lands the drone at home coordinates."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is landed")

    def hold(self):
        """Holds current position."""
        if debugPeriod.trigger() and fsmDebugging:
            logging.debug("Invalid command: Drone is descending")



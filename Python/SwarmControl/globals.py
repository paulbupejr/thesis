from pymavlink import mavutil
import logging
debugging = False  # Enable print debug statements

#  Periodic timers
sendPeriod = mavutil.periodic_event(1)
cluster_period = mavutil.periodic_event(1)  # Triggers the cluster calculations
portPeriod = mavutil.periodic_event(.33)  # Checks the available ports
debugPeriod = mavutil.periodic_event(1)
heartbeat_send = mavutil.periodic_event(1)
heartbeat_check_period = mavutil.periodic_event(0.33)

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )
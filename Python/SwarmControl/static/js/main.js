//var socket = io.connect('http://localhost:5000');

/*
$(document).ready(function(){
    socket.on('connect', function(msg) {
        console.log("Connected")
    });
 */
/*   socket.on('ports', function(msg) {
        //console.log("data")
        console.log(msg.data);
    });
*//*

*/
/*    socket.on('status', function(msg) {
        console.log(msg.data);
    });*//*


    $(".test").click(function(){
        socket.emit('get_ports', "token");
    });

});
*/
var simulator = [
    {
        "id": 1,
        "gps": [32.4054843, -81.7900138],
        "gps_fix": "GPS Fix",
        "alt": 18.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.4054843, -81.7900138],
        "voltage": 11,
        "batt": "90",
        "sats": 4,
        "port": "COM3",
        "state": "Operation"
    },
    {
        "id": 2,
        "gps": [32.4077301, -81.7900138],
        "gps_fix": "GPS Fix",
        "alt": 19.8,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.4077301, -81.7900138],
        "voltage": 11.2,
        "batt": "92",
        "sats": 4,
        "port": "COM4",
        "state": "Operation"
    },
    {
        "id": 3,
        "gps": [32.406607179, -81.78771011],
        "gps_fix": "GPS Fix",
        "alt": 19.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.406607179, -81.78771011],
        "voltage": 11.6,
        "batt": "97",
        "sats": 4,
        "port": "COM6",
        "state": "Operation"
    },
    {
        "id": 4,
        "gps": [32.40436138, -81.7877101],
        "gps_fix": "GPS Fix",
        "alt": 19,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.40436138, -81.7877101],
        "voltage": 11,
        "batt": "95",
        "sats": 3,
        "port": "COM7",
        "state": "Operation"
    }];

var simulator2 = [
    {
        "id": 1,
        "gps": null,
        "gps_fix": "GPS Fix",
        "alt": 18.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.4054843, -81.7900138],
        "voltage": 11,
        "batt": "9",
        "sats": 4,
        "port": "COM3",
        "state": "Operation"
    },
    {
        "id": 2,
        "gps": null,
        "gps_fix": "GPS Fix",
        "alt": 19.8,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.4077301, -81.7900138],
        "voltage": 11.2,
        "batt": "92",
        "sats": 4,
        "port": "COM4",
        "state": "Operation"
    },
    {
        "id": 3,
        "gps": null,
        "gps_fix": "GPS Fix",
        "alt": 19.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.406607179, -81.78771011],
        "voltage": 11.6,
        "batt": "97",
        "sats": 4,
        "port": "COM6",
        "state": "Operation"
    },
    {
        "id": 4,
        "gps": null,
        "gps_fix": "GPS Fix",
        "alt": 19.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.406607179, -81.78771011],
        "voltage": 11.6,
        "batt": "97",
        "sats": 4,
        "port": "COM6",
        "state": "Operation"
    },
    {
        "id": 5,
        "gps": null,
        "gps_fix": "GPS Fix",
        "alt": 19.5,
        "alt2": 20,
        "mode": "Loiter",
        "armed": "Armed",
        "dest": [32.406607179, -81.78771011],
        "voltage": 11.6,
        "batt": "97",
        "sats": 4,
        "port": "COM6",
        "state": "Operation"
    }];

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

var packet = [];
var center = {
    lat: 32.4054843,
    lng: -81.7900138
};

var separationDistance = 500;
var operatingHeight = 10;
var ceilingHeight = 50;
var fenceRadius = 500;
var simNumber = 7;

var app = angular.module("app", ['btford.socket-io']).
    factory('mySocket', function (socketFactory) {
        var myIoSocket = io.connect('http://localhost:5000');
        console.log(window.location.host);
        mySocket = socketFactory({
            ioSocket: myIoSocket
        });
        return mySocket;
    });

var mapDiv = $("#page-content-wrapper").get(0);
//var mapDiv = $("#page-content-wrapper").get(0);

app.controller("swarmcontrol", function($scope,mySocket){

    $scope.separationDistance = separationDistance;
    $scope.operatingHeight = operatingHeight;
    $scope.ceilingHeight = ceilingHeight;
    $scope.fenceRadius = fenceRadius;
    $scope.simNumber = simNumber;

    mySocket.on("connect", function(msg) {
        console.log("Connected to Script")
    });

    $scope.drones = [];
    //var newPacket =  simulator2;


/*    //$scope.drones = [];
    console.log(_.size($scope.drones)); */

/*    setInterval(function () {
        if(_.size($scope.drones) > _.size(newPacket)) {
            console.log("Delete");
            var test = _.filter($scope.drones, function(drone){

                return _.find(newPacket, function(id){ return id.id == drone.id} )
            });
            test = _.each(test, function(d){
                return _.extend(d, _.find(newPacket, function(id){ return id.id == d.id} ));
            });
            $scope.drones = test;
            console.log(test);
        } else if(_.size($scope.drones) < _.size(newPacket)) {
            console.log("Add");

            var test = _.filter(newPacket, function(drone){

                if(_.isUndefined(_.find($scope.drones, function(d){return d.id == drone.id;})))
                {
                    return true
                } else {
                    return false
                }
            });
            _.each(test, function(d){$scope.drones.push(d)});
            //console.log(test);
        } else {
            $scope.drones = _.extend($scope.drones, _.find(newPacket, function(id){ return id.id == d.id} ))
        }
        $scope.$apply();
    }, 1000);*/




    mySocket.on('status', function(msg) {
        $scope.drones =  msg.data;
        $scope.$apply();

        //var newPacket =  msg.data;

        for (var j = 0; j < $scope.markers.length; j++) {
            $scope.markers[j].setMap(null);
        }
        for (var i = 0; i < $scope.drones.length; i++){
            createMarker($scope.drones[i]);
        }
    });

    mySocket.on('simDrones', function(msg) {
        $scope.drones =  msg.data;
        //var newPacket =  msg.data;

        for (var j = 0; j < $scope.markers.length; j++) {
            $scope.markers[j].setMap(null);
        }
        for (var i = 0; i < $scope.drones.length; i++){
            createMarker($scope.drones[i]);
        }
    });


    $scope.center = center;

    /* *********** Map Info *********** */
    var mapOptions = {
        center: new google.maps.LatLng($scope.center.lat,$scope.center.lng),
        zoom: 19,
        mapTypeId: google.maps.MapTypeId.TERRAIN
    };

    $scope.map = new google.maps.Map(mapDiv, mapOptions);
    $scope.markers = [];
    var infoWindow = new google.maps.InfoWindow();

    var image = {
        url: 'img/drone_blue.png',
        size: new google.maps.Size(60, 60),
        origin: new google.maps.Point(0,0),
        // The anchor for this image is the base of the flagpole at 0,32.
        anchor: new google.maps.Point(30, 30)
    };

    var fence = new google.maps.Circle({
        map: $scope.map,
        clickable: false,
        editable: true,
        radius: $scope.fenceRadius, //meters
        center: $scope.center,
        fillColor: '#d9534f',
        fillOpacity: .2,
        strokeColor: '#d43f3a',
        strokeOpacity: .4,
        strokeWeight: .8
    });

    var homeMarker = new google.maps.Marker({
        map: $scope.map,
        position: new google.maps.LatLng($scope.center.lat, $scope.center.lng),
        draggable:true,
        title: "MAV "
    });

    fence.bindTo('center', homeMarker, 'position');

    google.maps.event.addListener(homeMarker, 'dragend', function(a) {
        $scope.center = {lat: Number(a.latLng.lat().toFixed(7)), lng: Number(a.latLng.lng().toFixed(7))};
        $scope.$apply();
    });

    google.maps.event.addListener(fence, 'radius_changed', function(a) {
        $scope.fenceRadius = Math.round(fence.getRadius());
    });

    var createMarker = function (drone){

        var marker = new google.maps.Marker({
            map: $scope.map,
            position: new google.maps.LatLng(Number(drone.gps[0]), Number(drone.gps[1])),
            //position: new google.maps.LatLng(Number(drone.dest[0]), Number(drone.dest[1])),
            icon: image,
            title: "MAV " + drone.id
        });

        marker.content = '<div class="infoWindowContent">' +
        '<strong>Satellites:</strong> ' + drone.sats + '<br />' +
        '<strong>Location:</strong> ' + Number(drone.dest[0]) +',' + Number(drone.dest[1]) + '<br />' +
        '<strong>Destination:</strong> ' + Number(drone.dest[0]) +',' + Number(drone.dest[1])
        + '</div>';
        google.maps.event.addListener(marker, 'click', function(){
            infoWindow.setContent('<h3 class="text-center">' + marker.title + ' - ' + drone.state + '</h3>' + marker.content);
            infoWindow.open($scope.map, marker);
        });
        //console.log(marker);
/*        marker.content = '<div class="infoWindowContent"> Satellies: ' + info.sats + '</div>';

        google.maps.event.addListener(marker, 'click', function(){
            infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
            infoWindow.open($scope.map, marker);
        });*/

        $scope.markers.push(marker);
    };




    //Ports
    $scope.baudrates = [57600, 112500];
    $scope.ports = [];
    mySocket.on('ports', function(msg) {
        console.log("ports");
        $scope.ports = msg.data;
    });

    $scope.getPorts = function() {
        mySocket.emit("get_ports", "Update Ports");
        console.log("Update");
    };

    $scope.addDrone = function($event) {
        var port = $scope.comport;
        var baud = $scope.baud;
        var payload = {"port": port, "baud": baud};

        if(baud && port) {
            mySocket.emit('connect', payload);
            console.log(payload)
        }
    };

    $scope.launch = function() {
        console.log("Launch");
    };

    $scope.simulationInit = function() {

        var payload = {
            "lat": $scope.center.lat,
            "lon": $scope.center.lng,
            "separation": $scope.separationDistance,
            "drones": $scope.simNumber
        };

        console.log("Simulate");
        mySocket.emit("simulate", payload);

/*        $scope.drones = simulator;
        for (var i = 0; i < $scope.drones.length; i++){
            createMarker($scope.drones[i]);
        }*/
    };

    $scope.pause = function() {
        console.log("Pause");
    };

    $scope.recall = function() {
        console.log("Recall")
    };

    $scope.initiate = function() {
        mySocket.emit("initiate", "Initiate Sequence")
    };

    $scope.sendConfig = function() {

        fence.setRadius($scope.fenceRadius);
        var payload = {
                "lat": $scope.center.lat,
                "lon": $scope.center.lng,
                "fenceRadius": $scope.fenceRadius,
                "fenceHeight": $scope.ceilingHeight,
                "operatingHeight": $scope.operatingHeight,
                "separation": $scope.separationDistance
            };


        mySocket.emit("config", payload);

    };

    $scope.sendMode = function() {

    };

    $scope.targetField = null;
    $scope.focusCallback = function($event) {
        if($event.target === null) {
            return;
        }
        $scope.targetField = $event.target;
    };
    $scope.commandSend = function($event) {
        var droneId = null;
        var action = null;
        var payload = null;
        if(typeof($event) != "string"){
             action = $event.target.dataset.command;
             droneId = $event.target.parentNode.parentNode.id;
             payload = {};
            switch(action) {

                case "Arm":
                case "Disarm":
                case "Disconnect":
                {
                    payload = {"command": action, "drone": droneId};
                    console.log(payload);
                    mySocket.emit('command', payload);
                    break;
                }
                default:
                    return;
            }
        } else {
             droneId = $scope.targetField.parentNode.parentNode.parentNode.id;
             action = $event;
            payload = {"mode": action, "drone": droneId};
            console.log(payload);
            mySocket.emit('mode', payload);

        }

    };

    /*
     setInterval(function () {
     $http.get("/_sensors").success(function(data){
     console.log(data);
     app.alts = data
     })

     }, 1000);
     */


});
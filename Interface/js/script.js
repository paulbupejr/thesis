var radius = 1;
var launchPoint = {
    lat: 33.423253,
    lng: -83.786715
}


//32.423236, -81.786515
var mapOptions = {
    center: new google.maps.LatLng(launchPoint.lat, launchPoint.lng),
    zoom: 15,
    mapTypeId: 'hybrid'
};
var mapDiv = $('.main').get(0);
var map = new google.maps.Map(mapDiv, mapOptions);


var drone1 = new Drone(0,1,"Drone 1");
var drone2 = new Drone(60,2,"Drone 2");
var drone3 = new Drone(120,3,"Drone 3");
var drone4 = new Drone(180,4,"Drone 4");
var drone5 = new Drone(240,5,"Drone 5");
var drone6 = new Drone(300,6,"Drone 6");

$(document).ready(function(){

    initialize();
});

function initialize() {

    var center = launchPoint;

    new google.maps.Marker({
        position: center,
        map: map,
        title: this.name
    });

    new google.maps.Circle({
        map: map,
        clickable: false,
        // metres
        radius: 800,
        center: center,
        fillColor: '#0087DF',
        fillOpacity: .2,
        strokeColor: '#313131',
        strokeOpacity: .4,
        strokeWeight: .8
    });

    drone1.draw();
    drone2.draw();
    drone3.draw();
    drone4.draw();
    drone5.draw();
    drone6.draw();
}

function Drone(bearing, id, name) {
    this.coordinate = getCoordinate(radius, bearing);
    this.id = id;
    this.name = name;
};

Drone.prototype.draw = function(){
    new google.maps.Marker({
        position: this.coordinate,
        map: map,
        title: this.name
    });

    new google.maps.Circle({
        map: map,
        clickable: false,
        // metres
        radius: 800,
        center: this.coordinate,
        fillColor: '#0087DF',
        fillOpacity: .2,
        strokeColor: '#313131',
        strokeOpacity: .4,
        strokeWeight: .8
    });
};

function toDecimal(degrees, minutes, seconds){
    degrees = Math.round(degrees);
    minutes = Math.round(minutes);
    seconds = Math.round(seconds);

    return degrees + (minutes  / 60.0) + (seconds  / 3600.0);
}

function toDegreeMinuteSecond(decimal){
    var coordinate = {};
    var degreesInt;
    var isNegative = false;

    if(decimal < 0) {
        isNegative = true;
        decimal = -1 * decimal;
    }
    var degrees = Math.floor(decimal);
    var minutes = Math.floor((decimal - degrees) * 60);
    var seconds = Math.round((((decimal - degrees) * 60) - minutes) * 60)

    coordinate.degrees = degrees;
    coordinate.minutes = minutes;
    coordinate.seconds = seconds;

    return coordinate;
}

function getCoordinate(distance, bearing) {
    var geo = {}
    var finLat, finLong;
    var lat = radians(Number(launchPoint.lat));
    var long = radians(Number(launchPoint.lng));
    var dist = Number(distance) / 6371;
    var brng = radians(Number(bearing));
    finLat = Math.asin(Math.sin(lat)*Math.cos(dist) + Math.cos(lat)*Math.sin(dist)*Math.cos(brng));
    finLong = long + Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat), Math.cos(dist)-Math.sin(lat)*Math.sin(finLat));
    finLong = (finLong+3*Math.PI) % (2*Math.PI) - Math.PI; // normalise to -180..+180º
    geo.lat = degrees(Number(finLat));
    geo.lng = degrees(Number(finLong));
    return geo;
}


function isFloat(n) {
    return n === +n && n !== (n|0);
}

function isInteger(n) {
    return n === +n && n === (n|0);
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */


/** Extend Number object with method to  trim whitespace from string
 *  (q.v. blog.stevenlevithan.com/archives/faster-trim-javascript) */
if (typeof String.prototype.trim == 'undefined') {
    String.prototype.trim = function() {
        return String(this).replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }
}

// Converts from degrees to radians.
function radians(degrees) {
    return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
function degrees(radians) {
    return radians * 180 / Math.PI;
};